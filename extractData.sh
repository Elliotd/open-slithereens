#!/bin/bash

OUTPUT_FOLDER="assets"

# Function to sanitize the file names
# Argument 1 is the working dir Argument 2 is prefix to remove
function SanitizeFileNames () {
  local WORKING_DIR=$1;
  local PREFIX=$2;

  # Remove spaces
  for file in ${WORKING_DIR}/${PREFIX}*; do mv "$file" "${file// /_}"; done
  # Remove prefix
  for file in $WORKING_DIR/${PREFIX}*; do mv "$file" "${WORKING_DIR}/${file#${WORKING_DIR}/${PREFIX}}";done;
}

# Copy all files to have better dasm output namesd
cp "Slithereens 1.0.0 ƒ/Slithereens Levels" levels
cp "Slithereens 1.0.0 ƒ/Slithereens Sprites" sprites
cp "Slithereens 1.0.0 ƒ/Slithereens Sounds" sounds
cp "Slithereens 1.0.0 ƒ/Slithereens Music" music
cp "Slithereens 1.0.0 ƒ/Slithereens Titles" titles

# One needs to install resource_dasm first
# Extract data from their resource forks
mkdir ${OUTPUT_FOLDER}
mkdir ${OUTPUT_FOLDER}/Levels
./resource_dasm/resource_dasm "levels" ./${OUTPUT_FOLDER}/Levels

mkdir ${OUTPUT_FOLDER}/Titles
./resource_dasm/resource_dasm "titles" ./${OUTPUT_FOLDER}/Titles

mkdir ${OUTPUT_FOLDER}/Sprites
./resource_dasm/resource_dasm "sprites" ./${OUTPUT_FOLDER}/Sprites

mkdir ${OUTPUT_FOLDER}/Sounds
./resource_dasm/resource_dasm "sounds" ./${OUTPUT_FOLDER}/Sounds

mkdir ${OUTPUT_FOLDER}/Music
./resource_dasm/resource_dasm "music" ./${OUTPUT_FOLDER}/Music

./resource_dasm/resource_dasm Slithereens\ 1.0.0\ ƒ/Slithereens ./${OUTPUT_FOLDER}/SlithereenApp
SanitizeFileNames "./${OUTPUT_FOLDER}/SlithereenApp" "Slithereens_"

# Removing old dev files
echo "Removed some earlier rtf files as they seemed to be from earlier devellopement, you can modify the extractData.sh to get the files." > ${OUTPUT_FOLDER}/Levels/README.txt
rm ./${OUTPUT_FOLDER}/Levels/*.rtf
# Sanitizing names
SanitizeFileNames "./${OUTPUT_FOLDER}/Levels" "levels_TEXT_"

echo "Bin files were SprD and .txt were SpRt tiles." > ${OUTPUT_FOLDER}/Sprites/README.txt
# Extract the sprites from the game using the sprite renderer tool
SanitizeFileNames "./${OUTPUT_FOLDER}/Sprites" "sprites_SprD_"
SanitizeFileNames "./${OUTPUT_FOLDER}/Sprites" "sprites_SpRt_"

# Extracting the sprites
# Dealing with the firework snake that has another color lookup table
mkdir ${OUTPUT_FOLDER}/Sprites/601_Firework_Snakes/
./resource_dasm/render_sprite --clut="${OUTPUT_FOLDER}/Titles/titles_clut_131_Enter Name (High Score).bin" --SprD="${OUTPUT_FOLDER}/Sprites/601_Firework_Snakes.bin" --output=${OUTPUT_FOLDER}/Sprites/601_Firework_Snakes/frame
rm ${OUTPUT_FOLDER}/Sprites/601_Firework_Snakes.bin
# Dealing with all the others
for file in ./${OUTPUT_FOLDER}/Sprites/*.bin; do echo "processing file ${file}"; mkdir ${file%.bin}; ./resource_dasm/render_sprite --clut=${OUTPUT_FOLDER}/Titles/titles_clut_128_Standard.bin --SprD="${file}" --output=${file%.bin}/frame; rm ${file}; done;
# combining the bmp files
# Since hole is damage it was done manually
#magick convert ./${OUTPUT_FOLDER}/Sprites/174_Hole/frame.bmp ./${OUTPUT_FOLDER}/Sprites/174_Hole.png
rm -rf ./${OUTPUT_FOLDER}/Sprites/174_Hole
for folder in ./${OUTPUT_FOLDER}/Sprites/*/; do magick convert "${folder}*" -append ${folder%/}.png; rm -rf ${folder%/}; done;

# Sanitizing names
SanitizeFileNames "./${OUTPUT_FOLDER}/Sounds" "sounds_snd_"

SanitizeFileNames "./${OUTPUT_FOLDER}/Music" "music_MADH_"

# Starting to sanitize names
SanitizeFileNames "./${OUTPUT_FOLDER}/Titles" "titles_PICT_"
mkdir ./${OUTPUT_FOLDER}/Titles/ppat
mv ./${OUTPUT_FOLDER}/Titles/titles_ppat_* ./${OUTPUT_FOLDER}/Titles/ppat/
SanitizeFileNames "./${OUTPUT_FOLDER}/Titles/ppat" "titles_ppat_"
SanitizeFileNames "./${OUTPUT_FOLDER}/Titles" "titles_STR#_"

# Concatenate all the about strings to make it a single text files for the credits
(for file in ./${OUTPUT_FOLDER}/Titles/*About_strings_{0..61}.txt; do echo "$(cat $file)"; rm "$file";  done;) > ./${OUTPUT_FOLDER}/Titles/About_string.txt
# Removing all tiled files as they are redundant information and quite big
rm ./${OUTPUT_FOLDER}/Titles/ppat/*tiled.bmp
# Removing all bitmaps as they are all white/ones
rm ./${OUTPUT_FOLDER}/Titles/ppat/*bitmap.bmp

# Removing all the color table as they were all used already to get the files we wanted
rm ./${OUTPUT_FOLDER}/Titles/*clut*
rm ./${OUTPUT_FOLDER}/Titles/*pltt*


# Change all the bmp into png for lighter size :
for file in ./${OUTPUT_FOLDER}/Titles/*.bmp; do gm convert "$file" "${file%bmp}png"; rm "$file"; done;
for file in ./${OUTPUT_FOLDER}/Titles/ppat/*.bmp; do gm convert "$file" "${file%bmp}png"; rm "$file"; done;

# Process the tiles so that the shadow and transparency is correct
for tilefile in ./${OUTPUT_FOLDER}/Titles/10*.png; do magick $tilefile -transparent white $tilefile; magick $tilefile -fill 'rgba(1,1,1,0.3)' -opaque '#FF00FF' $tilefile; done;
# do the same operation for the barn tiles
magick ./${OUTPUT_FOLDER}/Titles/1100_Barn.png -transparent white ./${OUTPUT_FOLDER}/Titles/1100_Barn.png
magick ./${OUTPUT_FOLDER}/Titles/1100_Barn.png -fill 'rgba(1,1,1,0.3)' -opaque '#FF00FF' ./${OUTPUT_FOLDER}/Titles/1100_Barn.png

# Delete all temporary files
rm levels sprites sounds music titles
