# Slithereens

# Original developper description

(found [here](https://download.cnet.com/Slithereens/3000-2095_4-4060.html))

Psychologists say that the limbic system controls the basic instincts for survival: feeding, fighting, self-preservation, and reproduction.
Now there's a game that taps directly into these primal instincts: Slithereens, the new wise-crackin', snake-slidin' game from Ambrosia Software.

Set in the backyard of the evil Dr. Funkengruven, Slithereens is an adventure that will satisfy your appetite for action. You are Luther, a peaceful snake who was surgically sassified and imbued with a hunger for snakemeat. Unfortunately, there are hundreds of ther snakes like you, each wanting the same thing: to munch on you!

Slide Luther through the rocky maze, while eating other snakes, rodents, birds, and other yummies. Avoid getting eaten, and protect your eggs from other animals. Taunt your enemies with disrespectful words. Survive, and live to munch another day. Fail, and... die, but you can always just start another game, right?

Your odds of survival are slim. Luther must face bigger, meaner, and smarter snakes as time goes by. Of course, you can easily cut your foe to an edible size by munching his tail or midsection. But watch out! If a larger snake bites your head, you're dead!

The jive-talking snakes must fight through five funky environments, each with its own varieties of obstacles and dangers. Over 100 digitized sounds breathe life into the funkadelic Luther. Vivid artwork adds spicy flavor to the game, garnished with a tasty soundtrack. But don't waste your time appreciatin' the scenery, or else those crafty snakes with sneakdafied AI will make you their supper!

Programmed by Jesse Liesch, Slithereens is the perfect game for all ages. It features thousands of frames of beautifully rendered 3D animation, an upbeat MOD soundtrack with 6 original tunes, a crockpot full of of spicy taunts, and play control that anyone can pick up in a second. Once you've been bitten, you'll really be smitten! There's no antidote for a game this addictin'!

# Usage

## Running the game

- Install rust
- `cargo run -r`

## Controls

 - P : Pass to next level

# Contributing
## Extract game resources

- Install [resource_dasm](https://github.com/fuzziqersoftware/resource_dasm)
- Download the [V1.0.0 of slithereens](https://www.macintoshrepository.org/5980-slithereens)
- Install imagemagick (brew or apt-get)
- Run extractData.sh to extract the resources from the original game files

## Resources

The [ResEdit reference](https://developer.apple.com/library/archive/documentation/mac/pdf/ResEditReference.pdf) book can be a good tool to find what different file format are in the program.

- actb Alert lookup table
- ALRT Alert template
- BNDL Bundle
- clut Generic color look up table
- CNTL Control template
- CODE Application code
- CURS are black and white cursors
- dctb dialog color lookup table
- DITL dialog item list
- DLOG dialog boxes
- FREF File reference
- ICN# Icon list
- MENU menu
- PICT Quick draw picture
- pltt Color palette
- ppat A pixel pattern
- SIZE Multi finder size information
- snd Sound
- STR# A string list (hence why I concatenated it back in the end)
- styl Style information for text edit
- TEXT Unlabled string
- TMPL ResEdit template
- vers Version
- WIND Window template

Original music are in MADH format, those sounds [already have been extracted to eaily readable file](https://www.youtube.com/watch?v=qfIRHoxJe8c).

Still unknown formats :

- cfrg
- DATA
- icl8
- SprD possibly in house bin for several sprites
- SpRt possibly in house text for animation
- SrpC
- tset
- WDEx

## Extracting sprites

Working out the right color palette for the image :

- Player 1 star has some ff (256) (black) and some a1 (161) that should be the inside of the star and be green.
- Standard clut has some green at 161
- About screen has some green at 161 but seems too dark
- Game over has some green but the title makes me think it's not right
- Enter name has blue at 161 so it's eliminated.
- Splash screen is brown at 161 not that one
- Ad is probably linked to the ad
- I'm a bit confused as to why have clut and pltt files

# Credits

All music extraction credit goes to [Berry's Game Music](https://www.youtube.com/channel/UCvg8LSn8WDAzvBRzCW1R1pA)

- [Fixed music](https://www.youtube.com/watch?v=VMUBFtbMUr0)
- [Unfixed music](https://www.youtube.com/watch?v=qfIRHoxJe8c&t=482s)
