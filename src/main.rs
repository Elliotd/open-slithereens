use game::GameManager;
use game::LevelInfo;
use macroquad::input;
use macroquad::prelude::*;
use renderer::MainMenuStatus;
use renderer::Renderer;
use rodio::OutputStream;
use rodio::OutputStreamHandle;
use sound_manager::SoundManager;
use std::path::Path;
use std::time::SystemTime;

mod game;
mod loader_util;
mod renderer;
mod sound_manager;

pub struct GameSettings<'a> {
    pub single_player: bool,
    pub current_level: usize,
    pub level_info: &'a LevelInfo,
}

/// Enum of the game status
enum ApplicationStatus<'a> {
    MainMenu,
    Playing(GameSettings<'a>),
}

/// button position for the main menu
/// Rectangle position then rectangle width and height
const BUTTON_POSITION: [(f32, f32, f32, f32, MainMenuStatus); 8] = [
    (90., 93., 87., 65., MainMenuStatus::Register),
    (321., 78., 77., 59., MainMenuStatus::Preference),
    (421., 92., 74., 69., MainMenuStatus::TwoPlayerGame),
    (461., 323., 90., 84., MainMenuStatus::OnePlayerGame),
    (587., 168., 82., 63., MainMenuStatus::Help),
    (592., 363., 98., 75., MainMenuStatus::HighScores),
    (223., 262., 86., 64., MainMenuStatus::Quit),
    (113., 324., 96., 74., MainMenuStatus::About),
];

#[macroquad::main("Texture")]
async fn main() {
    let (_stream, stream_handle) = OutputStream::try_default().unwrap();

    let (renderer, mut sound_manager, levels) = load_resources(stream_handle).await;

    let mut game_status = ApplicationStatus::Playing(GameSettings {
        single_player: true,
        current_level: 1,
        level_info: levels.get(0).unwrap(),
    });

    let mut game_status = ApplicationStatus::MainMenu;

    loop {
        match game_status {
            ApplicationStatus::MainMenu => {
                // get mouse position :
                let (mut mouse_pos_x, mut mouse_pos_y) = input::mouse_position();
                let screen_offset = renderer.get_top_left_pixel_of_frame();

                // Correct the menu position
                mouse_pos_x = mouse_pos_x - screen_offset.0;
                mouse_pos_y = mouse_pos_y - screen_offset.1;

                let mut menu_status: Option<MainMenuStatus> = None;

                for (x, y, w, h, status) in BUTTON_POSITION {
                    if mouse_pos_x > x
                        && mouse_pos_x < x + w
                        && mouse_pos_y > y
                        && mouse_pos_y < y + h
                    {
                        menu_status = Some(status);
                        break;
                    }
                }

                renderer.display_main_menu(
                    &menu_status,
                    input::is_mouse_button_down(input::MouseButton::Left),
                );

                if input::is_mouse_button_pressed(input::MouseButton::Left) {
                    match &menu_status {
                        Some(_) => sound_manager.play_sound(502), // Mouse clic
                        None => {}
                    }
                }

                if input::is_mouse_button_released(input::MouseButton::Left) {
                    match &menu_status {
                        Some(status) => match status {
                            MainMenuStatus::Register => {
                                eprintln!("Register button not implemented")
                            }
                            MainMenuStatus::Preference => {
                                eprintln!("Preference button not implemented")
                            }
                            MainMenuStatus::TwoPlayerGame => {
                                eprintln!("TwoPlayerGame button not implemented")
                            }
                            MainMenuStatus::OnePlayerGame => {
                                game_status = ApplicationStatus::Playing(GameSettings {
                                    single_player: true,
                                    current_level: 1,
                                    level_info: levels.get(0).unwrap(),
                                });
                            }
                            MainMenuStatus::Help => eprintln!("Help button not implemented"),
                            MainMenuStatus::HighScores => {
                                eprintln!("HighScores button not implemented")
                            }
                            MainMenuStatus::Quit => break,
                            MainMenuStatus::About => eprintln!("About button not implemented"),
                        },
                        None => {}
                    }
                }
            }
            ApplicationStatus::Playing(game_settings) => {
                let game_manager = GameManager::new(&game_settings);
                let mut last_frame_time = SystemTime::now();
                sound_manager.play_music(game_settings.level_info.music_id);
                loop {
                    game_manager.process_tick(last_frame_time.elapsed().unwrap());
                    renderer.render_game_frame(&game_manager);
                    last_frame_time = SystemTime::now();

                    if input::is_key_pressed(KeyCode::P) {
                        match levels.get(game_settings.current_level) {
                            Some(level) => {
                                let next_level = GameSettings {
                                    single_player: true,
                                    current_level: game_settings.current_level + 1,
                                    level_info: level, // The level vector starts at 0
                                };
                                game_status = ApplicationStatus::Playing(next_level);
                                break;
                            }
                            None => {
                                game_status = ApplicationStatus::MainMenu;
                                sound_manager.play_music(128); // Intro music
                                break;
                            }
                        }
                    }

                    next_frame().await;
                }
            }
        }
        next_frame().await
    }
}

pub async fn load_resources(
    stream_handle: OutputStreamHandle,
) -> (Renderer, SoundManager, Vec<LevelInfo>) {
    let loading_screen: Texture2D = load_texture("assets/Titles/128_Loading....png")
        .await
        .unwrap();

    draw_loading_frame(0., loading_screen).await;

    // Load music
    let mut sound_manager = SoundManager::init(stream_handle);

    sound_manager.load_sound_file(Path::new("assets/Sounds/502_Button_Click.wav"), 502);

    // fake loading screen
    for n in 1..100 {
        draw_loading_frame(n as f32, loading_screen).await;
    }

    // load textures
    let main_menu_image: Texture2D = load_texture("assets/Titles/129_Main_Screen_Resting.png")
        .await
        .unwrap();
    let main_menu_hover_image: Texture2D =
        load_texture("assets/Titles/132_Main_Screen_Highlited.png")
            .await
            .unwrap();
    let main_menu_pressed_image: Texture2D =
        load_texture("assets/Titles/131_Main_Screen_Clicked.png")
            .await
            .unwrap();
    let mut renderer = Renderer::init(
        main_menu_image,
        main_menu_hover_image,
        main_menu_pressed_image,
    );

    renderer
        .load_animated_sprites("128_Player_1_Head", 128)
        .await;
    renderer
        .load_animated_sprites("129_Player_1_Body", 129)
        .await;

    // Load wall types
    renderer
        .load_walls_sprites("assets/Titles/1000_Cavern_1.png", 1000)
        .await;
    renderer
        .load_walls_sprites("assets/Titles/1002_Cavern_2.png", 1002)
        .await;
    renderer
        .load_walls_sprites("assets/Titles/1004_Forest_1.png", 1004)
        .await;
    renderer
        .load_walls_sprites("assets/Titles/1006_Forest_2.png", 1006)
        .await;
    renderer
        .load_walls_sprites("assets/Titles/1008_Grass_1.png", 1008)
        .await;
    renderer
        .load_walls_sprites("assets/Titles/1010_Grass_2.png", 1010)
        .await;
    renderer
        .load_walls_sprites("assets/Titles/1012_Hedgemaze_1.png", 1012)
        .await;
    renderer
        .load_walls_sprites("assets/Titles/1014_Hedgemaze_2.png", 1014)
        .await;
    renderer
        .load_walls_sprites("assets/Titles/1016_Castle_1.png", 1016)
        .await;
    renderer
        .load_walls_sprites("assets/Titles/1018_Castle_2.png", 1018)
        .await;
    renderer
        .load_walls_sprites("assets/Titles/1100_Barn.png", 1100)
        .await;

    // Load background type
    renderer
        .load_background_sprites("assets/Titles/ppat/1000_Cavern_1.png", 1000)
        .await;
    renderer
        .load_background_sprites("assets/Titles/ppat/1001_Cavern_2.png", 1001)
        .await;
    renderer
        .load_background_sprites("assets/Titles/ppat/1002_Cavern_3.png", 1002)
        .await;
    renderer
        .load_background_sprites("assets/Titles/ppat/1003_Cavern_4.png", 1003)
        .await;
    renderer
        .load_background_sprites("assets/Titles/ppat/1004_Forest_1.png", 1004)
        .await;
    renderer
        .load_background_sprites("assets/Titles/ppat/1005_Forest_2.png", 1005)
        .await;
    renderer
        .load_background_sprites("assets/Titles/ppat/1006_Forest_3.png", 1006)
        .await;
    renderer
        .load_background_sprites("assets/Titles/ppat/1007_Forest_4.png", 1007)
        .await;
    renderer
        .load_background_sprites("assets/Titles/ppat/1008_Grass_1.png", 1008)
        .await;
    renderer
        .load_background_sprites("assets/Titles/ppat/1009_Grass_2.png", 1009)
        .await;
    renderer
        .load_background_sprites("assets/Titles/ppat/1010_Grass_3.png", 1010)
        .await;
    renderer
        .load_background_sprites("assets/Titles/ppat/1011_Grass_4.png", 1011)
        .await;
    renderer
        .load_background_sprites("assets/Titles/ppat/1012_Hedgemaze_1.png", 1012)
        .await;
    renderer
        .load_background_sprites("assets/Titles/ppat/1013_Hedgemaze_2.png", 1013)
        .await;
    renderer
        .load_background_sprites("assets/Titles/ppat/1014_Hedgemaze_3.png", 1014)
        .await;
    renderer
        .load_background_sprites("assets/Titles/ppat/1015_Hedgemaze_4.png", 1015)
        .await;
    renderer
        .load_background_sprites("assets/Titles/ppat/1016_Castle_1.png", 1016)
        .await;
    renderer
        .load_background_sprites("assets/Titles/ppat/1017_Castle_2.png", 1017)
        .await;
    renderer
        .load_background_sprites("assets/Titles/ppat/1018_Castle_3.png", 1018)
        .await;
    renderer
        .load_background_sprites("assets/Titles/ppat/1019_Castle_4.png", 1019)
        .await;
    renderer
        .load_background_sprites("assets/Titles/ppat/1100_Barn.png", 1100)
        .await;

    (renderer, sound_manager, loader_util::load_levels())
}

pub async fn draw_loading_frame(percent_complete: f32, loading_screen: Texture2D) {
    clear_background(BLACK);

    draw_texture(
        loading_screen,
        screen_width() / 2. - loading_screen.width() / 2.,
        screen_height() / 2. - loading_screen.height() / 2.,
        WHITE,
    );

    let loading_bar_x_offset = 182.0;
    let loading_bar_y_offset = 430.0;
    let loading_bar_height = 18.0;
    let max_bar_length = 280.0;
    draw_rectangle(
        screen_width() / 2. - loading_screen.width() / 2. + loading_bar_x_offset,
        screen_height() / 2. - loading_screen.height() / 2. + loading_bar_y_offset,
        max_bar_length * percent_complete / 100.0,
        loading_bar_height,
        GREEN,
    );
    next_frame().await;
}
