use rodio::source::{Buffered, SkipDuration, TakeDuration};
use rodio::{source::Source, Decoder, OutputStreamHandle, Sink};
use std::collections::HashMap;
use std::fs::File;
use std::io::BufReader;
use std::path::Path;
use std::time::Duration;

static OPENING_MUSIC: (u64, u64) = (0, 89000);
static MISC_MUSIC: (u64, u64) = (90000, 202000);
static WOODS_MUSIC: (u64, u64) = (203000, 349000);
static RIVER_MUSIC: (u64, u64) = (350000, 471000);
static HEDGE_MUSIC: (u64, u64) = (472000, 606000);
static CASTLE_MUSIC: (u64, u64) = (607000, 739000);

pub struct SoundManager {
    music_volume: f32,
    sound_volume: f32,
    master_volume: f32,
    stream_handle: OutputStreamHandle,
    music_sink: Option<Sink>,
    music_hashmap:
        HashMap<usize, Buffered<TakeDuration<SkipDuration<Buffered<Decoder<BufReader<File>>>>>>>,
    sound_hashmap: HashMap<usize, Buffered<Decoder<BufReader<File>>>>,
}
impl SoundManager {
    /// This initializes the sound manager, but also plays the start music before it finishes
    pub fn init(stream_handle: OutputStreamHandle) -> SoundManager {
        let mut sound_manager = SoundManager {
            music_volume: 1.0,
            sound_volume: 1.0,
            master_volume: 1.0, // CURRENTLY SET AT 0 SO IT DOES NOT BOTHER ME WHILE CODING
            stream_handle,
            music_sink: None,
            music_hashmap: HashMap::new(),
            sound_hashmap: HashMap::new(),
        };

        let file = BufReader::new(File::open("assets/Music/FixedOriginal320kbps.mp3").unwrap());
        // Decode that sound file into a source
        let music_sound = Decoder::new(file).unwrap().buffered();
        sound_manager.music_hashmap.insert(
            128,
            music_sound
                .clone()
                .skip_duration(Duration::from_millis(OPENING_MUSIC.0))
                .take_duration(Duration::from_millis(OPENING_MUSIC.1 - OPENING_MUSIC.0))
                .buffered(),
        );

        // Play the music while loading extracting the other music
        sound_manager.play_music(128);

        // Loading all other music
        sound_manager.music_hashmap.insert(
            1000,
            music_sound
                .clone()
                .skip_duration(Duration::from_millis(HEDGE_MUSIC.0))
                .take_duration(Duration::from_millis(HEDGE_MUSIC.1 - HEDGE_MUSIC.0))
                .buffered(),
        );
        sound_manager.music_hashmap.insert(
            1001,
            music_sound
                .clone()
                .skip_duration(Duration::from_millis(WOODS_MUSIC.0))
                .take_duration(Duration::from_millis(WOODS_MUSIC.1 - WOODS_MUSIC.0))
                .buffered(),
        );
        sound_manager.music_hashmap.insert(
            1002,
            music_sound
                .clone()
                .skip_duration(Duration::from_millis(RIVER_MUSIC.0))
                .take_duration(Duration::from_millis(RIVER_MUSIC.1 - RIVER_MUSIC.0))
                .buffered(),
        );
        sound_manager.music_hashmap.insert(
            1003,
            music_sound
                .clone()
                .skip_duration(Duration::from_millis(MISC_MUSIC.0))
                .take_duration(Duration::from_millis(MISC_MUSIC.1 - MISC_MUSIC.0))
                .buffered(),
        );
        sound_manager.music_hashmap.insert(
            1004,
            music_sound
                .clone()
                .skip_duration(Duration::from_millis(CASTLE_MUSIC.0))
                .take_duration(Duration::from_millis(CASTLE_MUSIC.1 - MISC_MUSIC.0))
                .buffered(),
        );
        return sound_manager;
    }

    pub fn load_sound_file(&mut self, path: &Path, id: usize) {
        let file = BufReader::new(File::open(path).unwrap());
        // Decode that sound file into a source
        let sample_sound = Decoder::new(file).unwrap().buffered();
        self.sound_hashmap.insert(id, sample_sound);
    }

    pub fn play_music(&mut self, id: usize) {
        match self.music_sink {
            Some(_) => self.stop_music(),
            None => {}
        }

        // Load a sound from a file, using a path relative to Cargo.toml
        let music_sink = Sink::try_new(&self.stream_handle).unwrap();

        music_sink.append(self.music_hashmap.get(&id).unwrap().clone());

        // The sound plays in a separate thread. This call will block the current thread until the sink
        // has finished playing all its queued sounds.
        music_sink.play();
        music_sink.set_volume(self.master_volume * self.music_volume);
        self.music_sink = Some(music_sink);
    }

    pub fn stop_music(&mut self) {
        // Load a sound from a file, using a path relative to Cargo.toml
        match &self.music_sink {
            Some(sink) => {
                sink.stop();
                self.music_sink = None;
            }
            None => {}
        }
    }

    pub fn play_sound(&self, sound_id: usize) {
        let source = self.sound_hashmap.get(&sound_id).unwrap();

        let sound_sink = Sink::try_new(&self.stream_handle).unwrap();
        sound_sink.append(source.clone());
        sound_sink.set_volume(self.master_volume * self.sound_volume);
        sound_sink.play();
        sound_sink.detach();
    }
}
