use std::{collections::hash_map, time::Duration};

use crate::GameSettings;

#[derive(Copy, Clone)]
pub enum MapTiles {
    NotImplemented,
    Wall,
    Ground,
    OutOfBounds,
    SimpleSnake,
    FastSnake,
    SmartSnake,
}

/// List of all tiles possible for the map.
#[derive(Copy, Clone)]
pub enum MazeTileType {
    None,
    UpDown,
    LeftRight,
    LeftDown,
    DownRight,
    UpRight,
    LeftUp,
    UpDownRight,
    UpLeftRight,
    LeftUpDown,
    LeftDownRight,
    UpDownLeftRight,
    Up,
    Down,
    Right,
    Left,
    Dot,
}

pub struct LevelInfo {
    /// Level definition
    level_tiles: [[MapTiles; 27]; 21],
    /// Music id
    pub music_id: usize,
    /// Background tiled texture
    pub background_texture_id: usize,
    /// Tile for the maze
    pub tiles_texture_id: usize,
    /// Called level type in the file, true if bonus level
    pub level_type: bool,
    /// average enemy size, not defined in every level
    pub average_enemy_length: Option<usize>,
}

impl LevelInfo {
    pub fn new(
        level_tiles: [[MapTiles; 27]; 21],
        music_id: usize,
        background_texture_id: usize,
        tiles_texture_id: usize,
        level_type: bool,
        average_enemy_length: Option<usize>,
    ) -> LevelInfo {
        LevelInfo {
            level_tiles,
            music_id,
            background_texture_id,
            tiles_texture_id,
            level_type,
            average_enemy_length,
        }
    }
}

pub struct GameManager<'a> {
    pub level_info: &'a LevelInfo,
    maze_tiles: [[MazeTileType; 27]; 21],
}

impl GameManager<'_> {
    pub fn new<'a>(settings: &'a GameSettings<'a>) -> GameManager<'a> {
        // First we generate the maze tiles used by the renderer to render the level.
        let mut maze_tiles: [[MazeTileType; 27]; 21] = [[MazeTileType::None; 27]; 21];
        for i in 0..21 {
            for j in 0..27 {
                let tile: MazeTileType;

                match settings.level_info.level_tiles[i][j] {
                    MapTiles::NotImplemented => {
                        eprintln!("Not implemented tile read in file at position ({j}, {i})");
                        tile = MazeTileType::None;
                        maze_tiles[i][j] = tile;
                    }
                    MapTiles::Wall => {
                        // Here we need to determine what kind of wall we have.
                        let mut has_wall_up = false;
                        let mut has_wall_down = false;
                        let mut has_wall_left = false;
                        let mut has_wall_right = false;

                        match settings.level_info.level_tiles[i].get(j + 1) {
                            Some(tile) => {
                                if matches!(tile, MapTiles::Wall) {
                                    has_wall_right = true;
                                }
                            }
                            None => {}
                        }
                        // Prevent substract overflow
                        if j > 0 {
                            match settings.level_info.level_tiles[i].get(j - 1) {
                                Some(tile) => {
                                    if matches!(tile, MapTiles::Wall) {
                                        has_wall_left = true;
                                    }
                                }
                                None => {}
                            }
                        }
                        match settings.level_info.level_tiles.get(i + 1) {
                            Some(line) => {
                                if matches!(line[j], MapTiles::Wall) {
                                    has_wall_down = true;
                                }
                            }
                            None => {}
                        }
                        // Prevent substract overflow
                        if i > 0 {
                            match settings.level_info.level_tiles.get(i - 1) {
                                Some(line) => {
                                    if matches!(line[j], MapTiles::Wall) {
                                        has_wall_up = true;
                                    }
                                }
                                None => {}
                            }
                        }

                        // Depending on the different wall types, we chose the right tile type
                        match (has_wall_down, has_wall_left, has_wall_right, has_wall_up) {
                            (true, true, true, true) => tile = MazeTileType::UpDownLeftRight,
                            (true, true, true, false) => tile = MazeTileType::LeftDownRight,
                            (true, true, false, true) => tile = MazeTileType::LeftUpDown,
                            (true, true, false, false) => tile = MazeTileType::LeftDown,
                            (true, false, true, true) => tile = MazeTileType::UpDownRight,
                            (true, false, true, false) => tile = MazeTileType::DownRight,
                            (true, false, false, true) => tile = MazeTileType::UpDown,
                            (true, false, false, false) => tile = MazeTileType::Down,
                            (false, true, true, true) => tile = MazeTileType::UpLeftRight,
                            (false, true, true, false) => tile = MazeTileType::LeftRight,
                            (false, true, false, true) => tile = MazeTileType::LeftUp,
                            (false, true, false, false) => tile = MazeTileType::Left,
                            (false, false, true, true) => tile = MazeTileType::UpRight,
                            (false, false, true, false) => tile = MazeTileType::Right,
                            (false, false, false, true) => tile = MazeTileType::Up,
                            (false, false, false, false) => tile = MazeTileType::Dot,
                        }
                        maze_tiles[i][j] = tile;
                    }
                    MapTiles::Ground => tile = MazeTileType::None,
                    MapTiles::OutOfBounds => tile = MazeTileType::None,
                    MapTiles::SimpleSnake => eprintln!("Simple snake not implemented yet"),
                    MapTiles::FastSnake => eprintln!("Fast snake not implemented yet"),
                    MapTiles::SmartSnake => eprintln!("Smart snake not implemented yet"),
                }
            }
        }
        GameManager {
            level_info: settings.level_info,
            maze_tiles,
        }
    }

    pub fn process_tick(&self, delta_tick: Duration) {}

    pub fn get_maze_tiles(&self) -> &[[MazeTileType; 27]; 21] {
        &self.maze_tiles
    }
}
