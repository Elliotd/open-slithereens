use macroquad::texture::Texture2D;
use macroquad::{prelude::*, texture};
use std::collections::HashMap;

use crate::game::GameManager;
use crate::loader_util;

/// Slithereen animation instance
pub struct SlithereenAnimation {
    // Should the animation should go back and forth (1..n-1, n, n-1, .., 1)
    pub animate_back_and_forth: bool,
    // width of the texture
    pub width: usize,
    // Maximum frame (to know how long each sprite is)
    pub maximum_frame: usize,
    // source png image that contains several animation frames
    pub texture: Texture2D,
    // List of possible animations on that images
    pub animations_vector: Vec<SlithereenAnimationFrames>,
}

impl SlithereenAnimation {
    // Render a selected frame at a specific position
    fn render_animation_frame(&self, animation_frame: usize, x: f32, y: f32) {
        let width = self.width as f32;
        let selected_sprite: Rect =
            Rect::new(0., (self.width * animation_frame) as f32, width, width);
        draw_texture_ex(
            self.texture,
            x,
            y,
            WHITE,
            DrawTextureParams {
                source: Some(selected_sprite),
                ..Default::default()
            },
        );
    }
}

/// Animation info for a single slithereen file
pub struct SlithereenAnimationFrames {
    /// Frame delay between each animation update
    pub frame_delay: usize,
    /// Frame delay before looping
    pub loop_delay: usize,
    /// Texture atlass frame number for the start of the animation
    pub frame_begin: usize,
    /// Texture atlass frame number for the stop of the animation
    pub frame_end: usize,
}

/// Variables linked to the main menu
/// Which button is your mouse on top of, the bool indicates if the mouse is pressed
pub enum MainMenuStatus {
    Register,
    Preference,
    TwoPlayerGame,
    OnePlayerGame,
    Help,
    HighScores,
    Quit,
    About,
}

pub struct Renderer {
    pub main_menu_image: Texture2D,
    pub main_menu_hover_image: Texture2D,
    pub main_menu_pressed_image: Texture2D,
    animated_sprite_vec: HashMap<usize, SlithereenAnimation>,
    background_sprite_vec: HashMap<usize, Texture2D>,
    map_tile_sprites_vec: HashMap<usize, Texture2D>,
}

impl Renderer {
    pub fn init(
        main_menu_image: Texture2D,
        main_menu_hover_image: Texture2D,
        main_menu_pressed_image: Texture2D,
    ) -> Renderer {
        Renderer {
            main_menu_image,
            main_menu_hover_image,
            main_menu_pressed_image,
            animated_sprite_vec: HashMap::new(),
            background_sprite_vec: HashMap::new(),
            map_tile_sprites_vec: HashMap::new(),
        }
    }

    pub async fn load_animated_sprites(&mut self, file_name: &str, id: usize) {
        let sprite_anmation = loader_util::load_slithereen_animation(file_name).await;
        self.animated_sprite_vec.insert(id, sprite_anmation);
    }

    pub async fn load_walls_sprites(&mut self, path: &str, id: usize) {
        let texture: Texture2D = texture::load_texture(path).await.unwrap();
        self.map_tile_sprites_vec.insert(id, texture);
    }

    pub async fn load_background_sprites(&mut self, path: &str, id: usize) {
        let texture: Texture2D = texture::load_texture(path).await.unwrap();
        self.background_sprite_vec.insert(id, texture);
    }

    pub fn render_game_frame(&self, game_manager: &GameManager) {
        clear_background(BLACK);

        // Calculate the frame size of the game to black out the sides
        let frame: Rect = Rect::new(
            screen_width() / 2. - self.main_menu_image.width() / 2.,
            screen_height() / 2. - self.main_menu_image.height() / 2.,
            screen_width() / 2. + self.main_menu_image.width() / 2.,
            screen_height() / 2. + self.main_menu_image.height() / 2.,
        );

        let menu_offset_x = screen_width() / 2. - self.main_menu_image.width() / 2.;
        let menu_offset_y = screen_height() / 2. - self.main_menu_image.height() / 2.;

        // Deal with the scale
        let game_scale: f32 = 1.22;
        let background = self
            .background_sprite_vec
            .get(&game_manager.level_info.background_texture_id)
            .expect("Invalid background ID");
        // Size of the background when scaled
        let background_destination = Vec2::new(
            background.width() * game_scale,
            background.height() * game_scale,
        );

        let maze_tile_size: f32 = 24.;
        let maze_tile_image = self
            .map_tile_sprites_vec
            .get(&game_manager.level_info.tiles_texture_id)
            .expect("Invalid tile ID");
        // Size of the maze tiles when scaled
        let maze_tile_destination =
            Vec2::new(maze_tile_size * game_scale, maze_tile_size * game_scale);

        // Draw the background
        let mut i: f32 = 0.;
        let mut j: f32 = 0.;
        while i * background_destination.x < maze_tile_destination.x * 27. {
            j = 0.;
            while j * background_destination.y < maze_tile_destination.y * 21. {
                draw_texture_ex(
                    *background,
                    menu_offset_x + i * background_destination.x,
                    menu_offset_y + j * background_destination.y,
                    WHITE,
                    DrawTextureParams {
                        dest_size: Some(background_destination),
                        ..Default::default()
                    },
                );
                j += 1.;
            }
            i += 1.;
        }
        draw_texture_ex(
            *background,
            menu_offset_x,
            menu_offset_y,
            WHITE,
            DrawTextureParams {
                dest_size: Some(background_destination),
                ..Default::default()
            },
        );

        // Redraw the border in black to make the black frame
        // (easier than trying to shorten the sprites with macroquad)
        draw_rectangle(
            menu_offset_x,
            menu_offset_y + maze_tile_destination.y * 21.,
            (i + 1.) * background_destination.x,
            (j + 1.) * background_destination.y,
            BLACK,
        );
        draw_rectangle(
            menu_offset_x + maze_tile_destination.x * 27.,
            menu_offset_y,
            (i + 1.) * background_destination.x,
            (j + 1.) * background_destination.y,
            BLACK,
        );

        // render each static tiles of the maze.
        let maze_tiles = game_manager.get_maze_tiles();

        for i in 0..21 {
            for j in 0..27 {
                let tile_to_render = maze_tiles[i][j];
                let mut subsprite_rect: Option<Rect> = None;
                match tile_to_render {
                    crate::game::MazeTileType::None => {
                        // Do nothing because there is nothing to draw
                    }
                    crate::game::MazeTileType::UpDown => {
                        subsprite_rect = Some(Rect::new(0., 0., 24., 24.));
                    }
                    crate::game::MazeTileType::LeftRight => {
                        subsprite_rect = Some(Rect::new(25. * 1., 0., 24., 24.))
                    }
                    crate::game::MazeTileType::LeftDown => {
                        subsprite_rect = Some(Rect::new(25. * 2., 0., 24., 24.))
                    }
                    crate::game::MazeTileType::DownRight => {
                        subsprite_rect = Some(Rect::new(25. * 3., 0., 24., 24.))
                    }
                    crate::game::MazeTileType::UpRight => {
                        subsprite_rect = Some(Rect::new(25. * 0., 25. * 1., 24., 24.))
                    }
                    crate::game::MazeTileType::LeftUp => {
                        subsprite_rect = Some(Rect::new(25. * 1., 25. * 1., 24., 24.))
                    }
                    crate::game::MazeTileType::UpDownRight => {
                        subsprite_rect = Some(Rect::new(25. * 2., 25. * 1., 24., 24.))
                    }
                    crate::game::MazeTileType::UpLeftRight => {
                        subsprite_rect = Some(Rect::new(25. * 3., 25. * 1., 24., 24.))
                    }
                    crate::game::MazeTileType::LeftUpDown => {
                        subsprite_rect = Some(Rect::new(25. * 0., 25. * 2., 24., 24.))
                    }
                    crate::game::MazeTileType::LeftDownRight => {
                        subsprite_rect = Some(Rect::new(25. * 1., 25. * 2., 24., 24.))
                    }
                    crate::game::MazeTileType::UpDownLeftRight => {
                        subsprite_rect = Some(Rect::new(25. * 2., 25. * 2., 24., 24.))
                    }
                    crate::game::MazeTileType::Up => {
                        subsprite_rect = Some(Rect::new(25. * 3., 25. * 2., 24., 24.))
                    }
                    crate::game::MazeTileType::Down => {
                        subsprite_rect = Some(Rect::new(25. * 0., 25. * 3., 24., 24.))
                    }
                    crate::game::MazeTileType::Right => {
                        subsprite_rect = Some(Rect::new(25. * 1., 25. * 3., 24., 24.))
                    }
                    crate::game::MazeTileType::Left => {
                        subsprite_rect = Some(Rect::new(25. * 2., 25. * 3., 24., 24.))
                    }
                    crate::game::MazeTileType::Dot => {
                        subsprite_rect = Some(Rect::new(25. * 3., 25. * 3., 24., 24.))
                    }
                }
                match subsprite_rect {
                    Some(_) => draw_texture_ex(
                        *maze_tile_image,
                        menu_offset_x + maze_tile_destination.x * j as f32,
                        menu_offset_y + maze_tile_destination.y * i as f32,
                        WHITE,
                        DrawTextureParams {
                            source: subsprite_rect,
                            dest_size: Some(maze_tile_destination),
                            ..Default::default()
                        },
                    ),
                    None => {}
                }
            }
        }
    }

    pub fn display_main_menu(&self, menu_status: &Option<MainMenuStatus>, button_pressed: bool) {
        clear_background(BLACK);

        let menu_offset_x = screen_width() / 2. - self.main_menu_image.width() / 2.;
        let menu_offset_y = screen_height() / 2. - self.main_menu_image.height() / 2.;

        draw_texture(self.main_menu_image, menu_offset_x, menu_offset_y, WHITE);

        match menu_status {
            Some(status) => {
                let selected_sprite: Rect;
                match status {
                    MainMenuStatus::Register => {
                        selected_sprite = Rect::new(72., 69., 119., 110.);
                    }
                    MainMenuStatus::Preference => {
                        selected_sprite = Rect::new(310., 68., 97., 78.);
                    }
                    MainMenuStatus::TwoPlayerGame => {
                        selected_sprite = Rect::new(409., 68., 110., 94.);
                    }
                    MainMenuStatus::OnePlayerGame => {
                        selected_sprite = Rect::new(442., 317., 114., 95.);
                    }
                    MainMenuStatus::Help => {
                        selected_sprite = Rect::new(576., 160., 124., 82.);
                    }
                    MainMenuStatus::HighScores => {
                        selected_sprite = Rect::new(581., 354., 119., 101.);
                    }
                    MainMenuStatus::Quit => {
                        selected_sprite = Rect::new(218., 251., 109., 83.);
                    }
                    MainMenuStatus::About => {
                        selected_sprite = Rect::new(97., 283., 125., 124.);
                    }
                }

                let texture: Texture2D;

                match button_pressed {
                    true => texture = self.main_menu_pressed_image,
                    false => texture = self.main_menu_hover_image,
                }

                draw_texture_ex(
                    texture,
                    menu_offset_x + selected_sprite.x,
                    menu_offset_y + selected_sprite.y,
                    WHITE,
                    DrawTextureParams {
                        source: Some(selected_sprite),
                        ..Default::default()
                    },
                );
            }
            None => {}
        }
    }

    pub fn get_top_left_pixel_of_frame(&self) -> (f32, f32) {
        let menu_offset_x = screen_width() / 2. - self.main_menu_image.width() / 2.;
        let menu_offset_y = screen_height() / 2. - self.main_menu_image.height() / 2.;
        (menu_offset_x, menu_offset_y)
    }
}
