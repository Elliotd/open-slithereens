tiles = 1100
ppat = 1100
foodTime = 1000000000
music = 128
levelType = 1
###########################
#.........................#
#.#.#.#.#.#.#.#.#.#.#.#.#.#
#.........................#
#.#.#.#.#.#.#.#.#.#.#.#.#.#
#.........................#
#.#.#.#.#.#.#.#.#.#.#.#.#.#
#........^.......!........#
#.#.#.#.#.#.#.#.#.#.#.#.#.#
#............m............#
#.#.#.#.#.#.#.#.#.#.#.#.#.#
#............m............#
#.#.#.#.#.#.#.#.#.#.#.#.#.#
#........!.......^........#
#.#.#.#.#.#.#.#.#.#.#.#.#.#
#.........................#
#.#.#.#.#.#.#.#.#.#.#.#.#.#
#.........................#
#.#.#.#.#.#.#.#.#.#.#.#.#.#
#.........................#
###########################

edit m food
   type = mongoose
   player = computer
   addLife = true
   attackGroup = 32
   group = 16
   speed * 475
   speed / 300
   ai = mongoose
   points = 500
   stopChance = 0
   avgStopTime = 0
end

edit ^ egg
   addLife = true
   player = player1
   hatchTime = 2000000000
   eggGroup = player1
   sprite = player1
   snakeGroup = 1
end

edit !  egg
   addLife = true
   player = player2
   hatchTime = 2000000000
   eggGroup = player2
   sprite = player2
   snakeGroup = 2
end

end
